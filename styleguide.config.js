const path = require('path');

module.exports = {
  title: 'Soikea UI Components',
  //components: './src/**/*.jsx',
  serverPort: 7001,

  sections: [
    {
      name: 'Documentation',
      content: 'doc/index.md',
      sections: [
        {
          name: 'Definition of done',
          content: 'doc/dod.md',
        }
      ],
    },
    {
      name: 'Components',
      components: function() {
        return [
          'Avatar', 'Button', 'Checkbox', 'Dropdown',
          'Input', 'Loader', 'Paper', 'Select',
          'SvgIcon', 'Tabs', 'Toggle', 'AutoComplete'
        ].map(function(name){
          return './src/' + name + '/' + name + '.jsx'
        })
      },
    },
    // {
    //   name: 'Tansec',
    //   components: function() {
    //     return [
    //       'InputConfiguration'
    //     ].map(function(name){
    //       return './src/' + name + '/' + name + '.jsx'
    //     })
    //   },
    // },
  ],

  showCode: false,
  assetsDir: './styleguide',
  template: 'styleguide/index.html',

  getComponentPathLine: function(componentPath) {
    const name = path.basename(componentPath, '.jsx');
    return 'import ' + name + ' from \'soikea-ui/lib/' + name + '\'';
  },

  updateWebpackConfig(webpackConfig) {
    const dirs = [
    	path.resolve(__dirname, 'package.json'),
    	path.resolve(__dirname, 'src'),
    	path.resolve(__dirname, 'styleguide'),
    ];

    // custom renderers and styles
    webpackConfig.resolve.alias['rsg-components/StyleGuide/StyleGuideRenderer'] =
      path.join(__dirname, 'styleguide/components/StyleGuide');
    webpackConfig.resolve.alias['rsg-components/ReactComponent/ReactComponentRenderer'] =
      path.join(__dirname, 'styleguide/components/ReactComponent');
    webpackConfig.resolve.alias['rsg-components/TableOfContents/TableOfContentsRenderer'] =
      path.join(__dirname, 'styleguide/components/TableOfContents');

    webpackConfig.module.loaders.push(
     // uses .babelrc
     {
        test: /\.jsx?$/,
        include: dirs,
        loader: 'babel'
     }, {
        test: /\.json$/,
        include: dirs,
        loader: 'json',
     }, {
    		test: /\.css$/,
    		include: dirs,
    		loader: 'style!css?modules&importLoaders=1',
    	}
    );
    return webpackConfig;
  }
};
