### Installation

Soikea UI is hosted as zipped npm package at http://soikea-ui.surge.sh/

```bash
$ npm install http://soikea-ui.surge.sh/soikea-ui-0.0.9.tgz --save
```

It is preferred to always use the latest version (`0.0.9` as of now).

### Raleway Font

Components have been designed with the Raleway font. It is best to have it installed.
The font can be downloaded at Google Fonts.

```html
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700" rel="stylesheet">
```
### Usage

Once the library has been installed individual components can be used by importing them
from their directory. Compiled components reside inside the `soikea-ui/lib` folder.

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import Button from 'soikea-ui/lib/Button';

const App = () => (
  <div>
    <Button>My Button</Button>
  </div>
);

ReactDOM.render(
  <App />,
  document.getElementById('app')
);
```

<hr />
