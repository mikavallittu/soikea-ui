When developing new components keep the following requirements in mind:

- Minimum `props` are defined as `required` and are introduced first in order in `propTypes`
- Props are commented
- Component has a test
- Basic responsive support: looks OK at `500px`
- Add `Readme.md` in the component's folder with short description and a code example.

<hr />
