Paper component is a simple  box container.

    const style = {
      height: 120,
      width: 120,
      margin: 20,
      textAlign: 'center',
      display: 'inline-block',
    };

    <div style={{backgroundColor: '#EAEFF2'}}>
      <Paper style={style} />
      <Paper style={style} zDepth={2} rounded={false} />
      <Paper style={style} zDepth={0} circle />
    </div>
