import Colors from '../styles/colors';
import Theme from '../styles/theme';
import Spacing from '../styles/spacing';

const styles = {
  'paper': {
    backgroundColor: 'white',
    padding: Spacing.gutter,
    boxSizing: 'border-box',
    fontFamily: Theme.fontFamily,
    WebkitTapHighlightColor: Colors.transparent,

    '&.rounded': {
      borderRadius: 3,
    },

    '&.circle': {
      borderRadius: '50%',
    }

  }
};

export default styles;
