import React from 'react';
import classNames from 'classnames';
import theme from '../styles/theme';
import transitions from '../styles/transitions';
import createStyleSheet from '../utils/createStyleSheet';
import { fade } from '../utils/colorManipulator';
import styles from './Paper.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className, rounded, circle){
  const classes = styleSheet.classes;

  return classNames(classes.paper || 'paper', {
    rounded,
    circle
  }, className);
}

const zDepthShadows = [
    [0, 0, 0, 1],
    [0, 2, 0, 1],
    [0, 2, 6, 0.94],
  ].map((d) => (
    `${d[0]}px ${d[1]}px ${d[2]}px ${fade(theme.colorShadow, d[3])}`
  ));

function getStyles(props) {
  const {
    transitionEnabled,
    zDepth,
  } = props;

  return {
    root: {
      transition: transitionEnabled && transitions.easeOut(),
      boxShadow: zDepthShadows[zDepth], // No shadow for 0 depth papers
    },
  };
}

class Paper extends React.Component {
  static propTypes = {
    /**
     * Children passed into the paper element.
     */
    children: React.PropTypes.node,

    /**
     * Set to true to generate a circlular paper container.
     */
    circle: React.PropTypes.bool,

    /**
     * By default, the paper container will have a border radius.
     * Set this to false to generate a container with sharp corners.
     */
    rounded: React.PropTypes.bool,

    /**
     * Override the inline-styles of the root element.
     */
    style: React.PropTypes.object,

    /**
     * Set to false to disable CSS transitions for the paper element.
     */
    transitionEnabled: React.PropTypes.bool,

    /**
     * This number represents the zDepth of the paper shadow.
     */
    zDepth: React.PropTypes.oneOf([0, 1, 2]),
  };

  static defaultProps = {
    circle: false,
    rounded: true,
    transitionEnabled: false,
    zDepth: 1,
  };

  render() {
    const {
      rounded,
      circle,
      className,
      style,
      transitionEnabled,
      zDepth,
      children,
      ...other,
    } = this.props;

    const styles = getStyles(this.props);
    const classes = getClasses(className, rounded, circle);

    return (
      <div
        {...other}
        style={Object.assign(styles.root, style)}
        className={classes}
      >
        {children}
      </div>
    );
  }
}

export default Paper;
