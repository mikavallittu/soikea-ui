Select allows you to choose multiple options from a list.

    const options = [
      { value: 1, label: 'One' },
      { value: 2, label: 'Two' },
      {
        type: 'group', name: 'group1', items: [
          { value: 3, label: 'Three' },
          { value: 4, label: 'Four' }
        ]
      },
      {
        type: 'group', name: 'group2', items: [
          { value: 5, label: 'Five' },
          { value: 6, label: 'Six' }
        ]
      }
    ];

    <div style={{marginBottom: 280}}>
      <Select labelText='Grouped options' options={options} placeholder='Select grouped...' inline />
    </div>  
