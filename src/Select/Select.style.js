import colors from '../styles/colors';
import theme from '../styles/theme';
import transitions from '../styles/transitions';

const styles = {
  'select': {
    fontFamily: theme.fontFamily,
    fontSize: theme.fontSizeBase,
    fontWeight: 400,
    position: 'relative',
    width: '100%',

    '& > .canister': {
      position: 'relative',
      width: 'auto',

      '& > .control': {
        position: 'relative',
        overflow: 'hidden',
        padding: '5px',
        minWidth: 256,
        height: 36,
        boxSizing: 'border-box',
        border: '1px solid',
        borderColor: theme.colorBorder,
        borderRadius: 2,
        outline: 'none',
        backgroundColor: 'white',
        transition: 'ease 250ms border-color',

        '& > .placeholder': {
          color: colors.cloudy,
          marginRight: 28,
          lineHeight: '24px',

          '& > span': {
            marginLeft: 4,
          },

          '& > .tag': {
            display: 'inline-flex',
            float: 'left',
            height: 24,
            backgroundColor: theme.colorBrandPrimary,
            color: 'white',
            fontWeight: 200,
            borderRadius: 2,
            justifyContent: 'center',
            alignItems: 'center',
            padding: '0 7px 0 5px',
            marginRight: 3,
            marginBottom: 6,

            '& > .icon': {
              width: 16,
              height: 16,
              margin: '0 0',
              cursor: 'pointer',
            },

            '& > span': {
              padding: '0 2px 0 0',
              fontSize: '13px',
            }
          }
        },

        '& > .arrow': {
          position: 'absolute',
          right: 6,
          top: 6
        },
      },

      '& > .label-text': {
        position: 'absolute',
        left: 0,
        width: '100%',
        fontSize: theme.fontSizeBase,
        lineHeight: theme.fontSizeBase + 'px',
        textAlign: 'left',
        paddingLeft: 1,
        transition: 'ease 250ms color',
        whiteSpace: 'nowrap',
        color: theme.fontColor,
        fontWeight: 600,
        top: 0,
        marginTop: -24,
      },

      '& > .help-text': {
        position: 'absolute',
        left: 0,
        width: '100%',
        bottom: -20,
        fontSize: theme.fontSizeSmall,
        lineHeight: theme.fontSizeSmall + 'px',
        textAlign: 'left',
        paddingLeft: 1,
        transition: 'ease 250ms color',
        whiteSpace: 'nowrap',
        color: colors.cloudy,
        fontWeight: 500,

        '&.error': {
          color: theme.colorBrandError
        }
      },
    },

    '& > .menu': {
      backgroundColor: 'white',
      border: '1px solid',
      borderColor: theme.colorBorder,
      borderRadius: '0 0 2px 2px',
      boxShadow: '0 1px 0 ' + theme.colorShadow,
      boxSizing: 'border-box',
      marginTop: 2,
      padding: '5px 0',
      maxHeight: 300,
      overflowY: 'auto',
      position: 'absolute',
      top: '100%',
      width: '100%',
      zIndex: '1000',
      display: 'none',

      '&.open': {
        display: 'block',
      },

      '& .no-results': {
        color: colors.grayLight,
        padding: '6px 12px',
      },

      '& .option': {
        padding: '4px 12px',
        margin: '2px 4px',
        borderRadius: 2,
        transition: 'ease 200ms background-color',

        '&:hover': {
          backgroundColor: theme.colorBackgroundLight,
        },

        '& > .check': {
          float: 'right',
          display: 'none',
          width: 20
        },

        '&.selected': {
          backgroundColor: theme.colorBrandPrimary,
          color: 'white',

          '& > .check': {
            display: 'inline-block'
          }
        }
      },

      '& .group': {
        padding: '6px 0',

        '& > header': {
          color: colors.grayLight,
          fontSize: '80%',
          textTransform: 'uppercase',
          padding: '4px 16px'
        }

      }
    },

    '&.disabled': {

      '& > .control': {
        opacity: 0.6,
        cursor: 'not-allowed',
        backgroundColor: '#ECF1F7',
      }
    }
  }
};

export default styles;
