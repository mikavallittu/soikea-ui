import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import createStyleSheet from '../utils/createStyleSheet';
import events from '../utils/events';
import dom from '../utils/dom';
import ArrowUpIcon from '../svg-icons/arrow-up';
import ArrowDownIcon from '../svg-icons/arrow-down';
import CheckIcon from '../svg-icons/check';
import CloseIcon from '../svg-icons/close';
import theme from '../styles/theme';
import styles from './Select.style';
import assign from 'lodash/assign';
import omit from 'lodash/omit';

const styleSheet = createStyleSheet(styles);

function getClasses(className, disabled){
  const classes = styleSheet.classes;

  return classNames(classes.select || 'select', {
    disabled,
  }, className);
}

function getStyles(props, state) {
  const styles = {
    root: {},
    canister: {},
    control: {},
    label: {},
    menu: {}
  };

  if (props.inline) {
    styles.root.display = 'inline-block';
  }

  if (props.labelText) {
    styles.canister.marginTop = 36;
  }

  if (props.helpText) {
    styles.canister.marginBottom = 28;
    styles.menu.marginTop = -26;
  }

  if (state.isOpen) {
    styles.control.borderColor = theme.colorBrandPrimary;
    styles.label.color = theme.colorBrandPrimary;
  }

  if (props.menuMaxHeight) {
    styles.menu.maxHeight = props.menuMaxHeight === true ? 'inherit' : props.menuMaxHeight;
  }
  
  // if (props.menuAutoHeight) {
  //   styles.menu.maxHeight = 'inherit';
  // }

  return styles;
}

function getOptions(options = []) {
  const output = [];
  options.forEach((item) => {
    if (typeof item !== 'object') {
      output.push({ value: item, label: item });
    } else {
      output.push(item);
    }
  });
  return output;
}

function getDefaultValues(items) {
  const selected = {};
  items.forEach(item => selected[item.value] = item);
  return selected;
}

function Tag(props) {
  const icon = <CloseIcon size='auto' color='white' />;

  const onClose = (ev) => {
    if (props.onClose) props.onClose();
  };

  return (
    <div className='tag'>
      <span className='icon icon-close' onClick={onClose}>{icon}</span>
      <span>{props.children}</span>
    </div>
  );
}

function Option(props) {
  const {
    label,
    selected,
    onClick,
    ...other
  } = props;

  const classes = classNames('option', { selected });
  const icon = <CloseIcon color='white' size='inherit' />;

  return (
    <div
      className={classes}
      onClick={props.onClick}
    >
      {label}
      <span className='check'>
        {icon}
      </span>
    </div>
  );
}

class Select extends Component {
  static propTypes = {
    /**
     * If true, the select element will be disabled.
     */
    disabled: React.PropTypes.bool,

    /**
     * Array of strings (or option objects). See the code example.
     */
    options: React.PropTypes.array,

    /**
     * If true, the select element will close when option is checked.
     */
    optionAutoClose: React.PropTypes.bool,

    /**
    * The label text to display at top.
    */
    labelText: React.PropTypes.node,

    /**
    * The help text to display at bottom.
    */
    helpText: React.PropTypes.node,

    /**
     * Placeholder text.
     */
    placeholder: React.PropTypes.string,

    /**
     * Display as `inline-block` element
     */
    inline: React.PropTypes.bool,

    /**
     * Override the inline-styles of the root element.
     */
    style: React.PropTypes.object,

    /**
     * Default value to be selected.
     */
    value: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.object,
    ]),

    /**
     * Callback function that is triggered when select's value changes. Function signature:
     * `(event, value, name) => {}`
     */
    onChange: React.PropTypes.func,

    /**
     * Name of the select input. Passed as third parameter to `onChange` callback.
     */
    name: React.PropTypes.string,
  };

  static defaultProps = {
    options: [],
    defaultValues: [],
    placeholder: 'Select...',
  };

  constructor(props) {
    super(props);

    this.state = {
      selected: getDefaultValues(this.props.defaultValues),
      isOpen: false,
    };

    this.mounted = true;
  }

  componentWillReceiveProps (newProps) {

  }

  componentDidMount () {
    events.on(document, 'click', this.handleDocumentClick.bind(this));
    events.on(document, 'touchend', this.handleDocumentClick.bind(this));
  }

  componentWillUnmount () {
    this.mounted = false;
    events.off(document, 'click', this.handleDocumentClick.bind(this));
    events.off(document, 'touchend', this.handleDocumentClick.bind(this));
  }

  handleMouseDown (ev) {
    if (ev.type === 'mousedown' && ev.button !== 0) return;

    // Close icon inside the Tag component? Then stop.
    const closeIcon = dom.hasParent(ev.target, 'icon icon-close');
    if (closeIcon) return;

    ev.stopPropagation();
    ev.preventDefault();

    if (!this.props.disabled) {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
  }

  setValue (selected, ev) {
    const newState = {
      selected
    };
    this.fireChangeEvent(newState, ev);
    this.setState(newState);
  }

  toggleOption(option, ev) {
    const key = String(option.value);
    let selected;

    if (this.state.selected[key] == undefined) {
      selected = assign({}, this.state.selected, {
        [key]: option
      });
    } else {
      selected = omit(this.state.selected, key);
    }

    this.setValue(selected, ev);


    if (this.props.optionAutoClose) {
      this.setState({ isOpen: false }); 
    }
  }

  fireChangeEvent (newState, ev) {
    if (this.props.onChange) {
      this.props.onChange(ev, newState, this.props.name);
    }
  }

  isSelected(option) {
    return !!this.state.selected[option.value];
  }

  renderOption (option) {
    const label = option.label || option.value || option;
    const key = option.value;

    return (
      <Option
        key={key}
        label={label}
        selected={this.isSelected(option)}
        onClick={this.toggleOption.bind(this, option)}
      />
    );
  }

  buildMenu () {
    const options = getOptions(this.props.options);
    const ops = options.map((option, i) => {
      if (option.type === 'group') {
        const groupTitle = (<header className='header'>{option.name}</header>);
        const _options = option.items.map((item) => this.renderOption(item));

        return (
          <div className='group' key={i}>
            {groupTitle}
            {_options}
          </div>
        );
      } else {
        return this.renderOption(option);
      }
    });

    return ops.length ? ops : <div className='no-results'>No options found</div>;
  }

  handleDocumentClick (ev) {
    if (this.mounted) {
      if (!ReactDOM.findDOMNode(this).contains(ev.target)) {
        this.setState({ isOpen: false })
      }
    }
  }

  render () {
    const {
      className,
      style,
      disabled,
      inline,
      labelText,
      helpText,
      placeholder
    } = this.props;

    const classes = getClasses(className, disabled);
    const styles = getStyles(this.props, this.state);

    let labelTextElement = null;
    let helpTextElement = null;

    if (labelText) {
      labelTextElement = <span style={styles.label} className='label-text'>
        {labelText}
      </span>;
    }

    if (helpText) {
      helpTextElement = <span style={styles.help} className='help-text'>
        {helpText}
      </span>;
    }

    let tags = [];
    for (const key in this.state.selected) {
      const option = this.state.selected[key];
      tags.push(
        <Tag
          key={key}
          onClose={this.toggleOption.bind(this, option)}>
          {option.label || option.value}
        </Tag>
      );
    }
    
    const menu = <div style={styles.menu}  className={classNames('menu', {'open': this.state.isOpen})}>
      {this.buildMenu()}
    </div>;

    const icon = this.state.isOpen
      ? <ArrowUpIcon color={theme.colorBrandPrimary} />
      : <ArrowDownIcon color={theme.colorBrandPrimary} />;

    const controlProps = {
      style: styles.control,
      onMouseDown: this.handleMouseDown.bind(this),
      onTouchEnd: this.handleMouseDown.bind(this),
    };

    return (
      <div className={classes} style={Object.assign(styles.root, style)}>
        <div className='canister' style={styles.canister}>
          {labelTextElement}
          <div className='control' {...controlProps}>
            <div className='placeholder'>
              {tags.length ? tags : <span>{placeholder}</span>}
            </div>
            <span className='arrow'>{icon}</span>
          </div>
          {helpTextElement}
        </div>
        {menu}
      </div>
    );
  }
}

export default Select;
