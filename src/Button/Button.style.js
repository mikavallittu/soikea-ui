import Colors from '../styles/colors';
import Theme from '../styles/theme';
import { darken } from '../utils/colorManipulator';

const styles = {
  'button': {
    backgroundColor: Colors.grayLighter,
    cursor: 'pointer',
    padding: '0 20px',
    display: 'inline-flex',
    alignItems: 'center',
    border: '1px solid gray',
    color: 'black',
    lineHeight: '34px',
    fontSize: Theme.fontSizeBase,
    borderRadius: Theme.borderRadiusBase,
    fontWeight: Theme.fontWeightLight,
    transition: 'ease 250ms background-color',

    '&:focus': {
      outline: 'none',
    },

    '&.primary': {
      color: 'white',
      backgroundColor: Theme.colorBrandPrimary,
      border: '1px solid ' + Theme.colorBrandPrimary,

      '&:hover': {
        backgroundColor: darken(Theme.colorBrandPrimary, 0.1),
      },
    },

    '&.error': {
      color: 'white',
      backgroundColor: Theme.colorBrandError,
      border: '1px solid ' + Theme.colorBrandError,

      '&:hover': {
        backgroundColor: darken(Theme.colorBrandError, 0.1),
      },
    },

    '&.outline': {
      color: Theme.colorBrandPrimary,
      backgroundColor: Colors.transparent,
      textTransform: 'uppercase',
      // lineHeight: '28px',
      fontSize: Theme.fontSizeSmall,
      fontWeight: Theme.fontWeightBold,
      border: '1px solid ' + Theme.colorBrandPrimary,
    },

    '&.link': {
      color: Theme.colorBrandPrimary,
      backgroundColor: Colors.transparent,
      borderColor: Colors.transparent,
      textDecoration: 'underline',
    },

    '&[disabled]': {
      opacity: 0.5,
      cursor: 'default',
    },

    '&.primary > svg': {
      fill: 'white !important',
      margin: '0 3px 0 -9px',
      width: '24px !important'
    }
  }
};

export default styles;
