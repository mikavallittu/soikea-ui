Use Buttons to invite users to make important actions on your app.

    <div>
      <Button>Nappula</Button>
      <hr />

      <Button primary>Lisää uusi laite</Button>
      <hr />

      <Button outline>Kaikki kohteet</Button>
      <hr />

      <Button disabled>Nappula</Button>
      <hr />

      <Button link>Linkki</Button>

      <hr />

      <Button error>Poista</Button>
    </div>
