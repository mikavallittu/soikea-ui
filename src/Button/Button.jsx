import React, { Component } from 'react';
import classNames from 'classnames';
import { createChildFragment } from '../utils/childUtils';
import createStyleSheet from '../utils/createStyleSheet';
import styles from './Button.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className, primary, outline, link, error) {
  const classes = styleSheet.classes;

  return classNames(classes.button || 'button', {
    primary,
    outline,
    link,
    error,
  }, className);
}

function getStyles(props) {
  return {
    root: {

    }
  };
}

class Button extends Component {

  static propTypes = {
    /**
     * Children passed into the loader element.
     */
    children: React.PropTypes.node.isRequired,

    /**
     * If true, the element will be disabled.
     */
    disabled: React.PropTypes.bool,

    /**
     * If true, the button will use the error (red) color.
     */
    error: React.PropTypes.bool,

    /**
     * If true, the button will use the primary color.
     */
    primary: React.PropTypes.bool,

    /**
     * If true, the button will use the outline styling.
     */
    outline: React.PropTypes.bool,

    /**
     * If true, the button will use the link styling.
     */
    link: React.PropTypes.bool,

    /**
     * Use this property to insert an icon.
     */
    icon: React.PropTypes.node,

    /**
     * Place icon before or after the passed children.
     */
    iconPosition: React.PropTypes.oneOf([
      'before',
      'after',
    ]),
  };

  static defaultProps = {
    iconPosition: 'before'
  };

  render() {
    const {
      children,
      icon,
      iconPosition,
      error,
      primary,
      outline,
      link,
      className,
      style,
      ...other,
    } = this.props;

    const styles = getStyles(this.props);
    const classes = getClasses(className, primary, outline, link, error);

    // Place icon before or after children.
    const childrenFragment = iconPosition === 'before' ?
    {
      icon,
      children
    } :
    {
      children,
      icon
    };

    const buttonChildren = createChildFragment(childrenFragment);

    return (
      <button
        {...other}
        style={Object.assign(styles.root, style)}
        className={classes}>
        {buttonChildren}
      </button>
    );
  }
}

export default Button;
