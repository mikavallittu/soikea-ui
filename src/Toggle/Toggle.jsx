import React, {Component, PropTypes} from 'react';
import classNames from 'classnames';
import transitions from '../styles/transitions';
import Paper from '../Paper';
import createStyleSheet from '../utils/createStyleSheet';
import colors from '../styles/colors';
import theme from '../styles/theme';
import styles from './Toggle.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className){
  const classes = styleSheet.classes;

  return classNames(classes.toggle || 'toggle', className);
}

function getStyles(props, state) {
  const {
    disabled,
  } = props;

  const toggleSize = 12;
  const toggleTrackWidth = 30;
  const padding = 3;

  const styles = {
    root: {
      display: 'inline-flex',
      alignItems: 'center'
    },
    label: {
      fontFamily: theme.fontFamily,
      margin: '0 9px',
      fontSize: 15,
      fontWeight: 400,
      verticalAlign: 'middle',
    },
    track: {
      position: 'relative',
      display: 'inline-flex',
      backgroundColor: '#F5F5F5',
      width: toggleTrackWidth,
      height: toggleSize,
      padding,
      boxSizing: 'content-box',
      boxShadow: 'inset 1px 1px 0 0 rgba(0, 0, 0, 0.06)',
      borderRadius: (toggleSize + 2 * padding) / 2,
      justifyContent: 'center',
      alignItems: 'center',
      //transition: '450ms ease background-color'
    },
    toggle: {
      position: 'absolute',
      backgroundColor: colors.red,
      //backgroundColor: colors.cloudy,
      top: padding,
      left: padding,
      width: toggleSize,
      height: toggleSize,
      borderRadius: toggleSize / 2,
      transition: '150ms ease left'
    }
  };

  if (state.switched) {
    styles.toggle.backgroundColor = colors.lime;
    styles.toggle.left = toggleTrackWidth - toggleSize + padding;
  }

  return styles;
}

class Toggle extends Component {
  static propTypes = {
    /**
     * Determines whether the Toggle is initially turned on.
     * **Warning:** This cannot be used in conjunction with `toggled`.
     * Decide between using a controlled or uncontrolled input element and remove one of these props.
     * More info: https://fb.me/react-controlled-components
     */
    defaultToggled: PropTypes.bool,

    /**
     *
     */
    toggled: PropTypes.bool,

    /**
     * Will disable the toggle if true.
     */
    disabled: PropTypes.bool,

    /**
     * Callback function that is fired when the toggle switch is toggled. Function signature:
     * <code>
     *    (event, isSwitched, name) => {}
     * </code>
     */
    onToggle: PropTypes.func,

    /**
     * Inline styles for the label element.
     */
    labelStyle: PropTypes.object,

    /**
     * The label text to display.
     */
    labelText: PropTypes.node,

    /**
     * Position of the label text.
     */
    labelPosition: PropTypes.oneOf(['left', 'right']),

    /**
     * Name for the toggle element.
     */
    name: PropTypes.string,

  };

  static defaultProps = {
    disabled: false,
    labelPosition: 'right',
  };

  state = {
    switched: false,
  };

  componentWillMount() {
    const {toggled, defaultToggled} = this.props;

    if (toggled || defaultToggled) {
      this.setState({
        switched: true,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.handleStateChange(nextProps.toggled);
  }

  handleStateChange = (newSwitched) => {
    this.setState({
      switched: newSwitched,
    });
  };

  handleToggle = (event, isSwitched) => {
    if (this.props.onToggle) {
      this.props.onToggle(event, isSwitched, this.props.name);
    }
  };

  render() {
    const {
      defaultToggled,
      labelText,
      labelPosition,
      labelStyle,
      onToggle,
      toggled,
      style,
      className,
      ...other
    } = this.props;

    const styles = getStyles(this.props, this.state);
    const classes = getClasses(className);

    let labelElement;
    if (labelText) {
      labelElement = <span style={Object.assign(styles.label, labelStyle)}>
        {labelText}
      </span>;
    }

    return (
      <div style={styles.root} className={classes}>
        {labelPosition === 'left' && labelElement}
        <div
          style={Object.assign(styles.track, style)}
          onClick={this._onClick.bind(this)}>
          <span style={styles.toggle} />
        </div>
        {labelPosition === 'right' && labelElement}
      </div>
    );
  }

  _onClick(e) {
    if (this.props.disabled) {
      return;
    }
    const switched = !this.state.switched;
    this.handleStateChange(switched);
    this.handleToggle(e, switched);
  }
}

export default Toggle;
