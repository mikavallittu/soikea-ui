Toggle can be used as an on/off control.

    <div>
      <Toggle defaultToggled />

      <hr />

      <Toggle labelText='Label right' />

      <hr />

      <Toggle toggled={true} labelText='Label left' labelPosition='left' />
    </div>
