/**
 * Inspired by: https://github.com/fraserxu/react-dropdown
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import createStyleSheet from '../utils/createStyleSheet';
import events from '../utils/events';
import ArrowUpIcon from '../svg-icons/arrow-up';
import ArrowDownIcon from '../svg-icons/arrow-down';
import CheckIcon from '../svg-icons/check';
import theme from '../styles/theme';
import styles from './Dropdown.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className, disabled){
  const classes = styleSheet.classes;

  return classNames(classes.dropdown || 'dropdown', {
    'is-disabled': disabled,
  }, className);
}

function getStyles(props, state) {
  const styles = {
    root: {},
    canister: {},
    control: {},
    label: {},
    menu: {}
  };

  if (props.inline) {
    styles.root.display = 'inline-block';
  }

  if (props.labelText) {
    styles.canister.marginTop = 36;
  }

  if (props.helpText || (props.error && props.errorText)) {
    styles.canister.marginBottom = 28;
    styles.menu.marginTop = -26;
  }

  if (state.isOpen) {
    styles.control.borderColor = theme.colorBrandPrimary;
    styles.label.color = theme.colorBrandPrimary;
  }

  if (props.error) {
    styles.control.borderColor = theme.colorBrandError;
  }

  if (props.error && state.isOpen) {
    styles.label.color = theme.colorBrandError;
  }

  return styles;
}

class Dropdown extends Component {
  static propTypes = {
    /**
     * If true, the dropdown element will be disabled.
     */
    disabled: React.PropTypes.bool,

    /**
     * Set the error state
     */
    error: React.PropTypes.bool,

    /**
    * The error text to display at bottom. Shown only if error is `true`.
    */
    errorText: React.PropTypes.node,

    /**
     * Array of strings (or option objects). See the code example.
     */
    options: React.PropTypes.array,

    /**
    * The label text to display at top.
    */
    labelText: React.PropTypes.node,

    /**
    * The help text to display at bottom. Overridden by `errorText`.
    */
    helpText: React.PropTypes.node,

    /**
     * Placeholder text.
     */
    placeholder: React.PropTypes.string,

    /**
     * Display as `inline-block` element
     */
    inline: React.PropTypes.bool,

    /**
     * Override the inline-styles of the root element.
     */
    style: React.PropTypes.object,

    /**
     * Set the value for the dropdown.
     */
    value: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),

    /**
     * Callback function that is triggered when dropdown's value changes. Function signature:
     * `(event, value, name) => {}`
     */
    onChange: React.PropTypes.func,

    /**
     * Name of the input. Passed as third parameter to `onChange` callback.
     */
    name: React.PropTypes.string,
  };

  static defaultProps = {
    options: [],
    placeholder: 'Select...',
  };

  constructor(props) {
    super(props);

    const selected = this.getOptionByValue(props.options, props.value) || {
      label: props.placeholder,
      value: ''
    };

    this.state = {
      selected,
      isOpen: false
    }

    this.mounted = true;
  }

  getOptionByValue(options, value) {
    for (let i = 0; i < options.length; i++) {
      let option = options[i];
      if (option.type === 'group') {
        let opt = this.getOptionByValue(option.items, value);
        if (opt !== false) return opt;
      } else {
        if (typeof option === 'object' && option.value === value) return option;
        if (option === value) return option;
      }
    }
    return false;
  }

  componentWillReceiveProps (newProps) {
    const selected = this.getOptionByValue(newProps.options, newProps.value) || {
      label: newProps.placeholder,
      value: ''
    };
    this.setState({ selected });
  }

  componentDidMount () {
    events.on(document, 'click', this.handleDocumentClick.bind(this));
    events.on(document, 'touchend', this.handleDocumentClick.bind(this));
  }

  componentWillUnmount () {
    this.mounted = false;
    events.off(document, 'click', this.handleDocumentClick.bind(this));
    events.off(document, 'touchend', this.handleDocumentClick.bind(this));
  }

  handleMouseDown (ev) {
    if (ev.type === 'mousedown' && ev.button !== 0) return;
    ev.stopPropagation();
    ev.preventDefault();

    if (!this.props.disabled) {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
  }

  setValue (value, label, ev) {
    const newState = {
      selected: {
        value,
        label
      },
      isOpen: false
    };
    this.dirty = true;
    this.fireChangeEvent(newState, ev);
    this.setState(newState);
  }

  fireChangeEvent (newState, ev) {
    if (this.props.onChange) {
      this.props.onChange(ev, newState.selected.value, this.props.name);
    }
  }

  isEqualToSelected(option) {
    const { selected } = this.state;
    if (typeof option !== 'object') {
      return option === (selected.value != undefined ? selected.value : selected);
    }
    return option.value === selected.value;
  }

  renderOption (option) {
    const optionClass = classNames('option', {
      'selected': this.isEqualToSelected(option)
    });

    const value = option.value || option;
    const label = option.label || option.value || option;
    const icon  = <CheckIcon color='white' size='inherit' />;

    return (
      <div
        key={value}
        className={optionClass}
        onMouseDown={this.setValue.bind(this, value, label)}
        onClick={this.setValue.bind(this, value, label)}>
        {label}
        <span className='check'>
          {icon}
        </span>
      </div>
    )
  }

  buildMenu () {
    const { options } = this.props;
    const ops = options.map((option) => {
      if (option.type === 'group') {
        const groupTitle = (<header className='header'>{option.name}</header>);
        const _options = option.items.map((item) => this.renderOption(item));

        return (
          <div className='group' key={option.name}>
            {groupTitle}
            {_options}
          </div>
        );
      } else {
        return this.renderOption(option);
      }
    });

    return ops.length ? ops : <div className='no-results'>No options found</div>;
  }

  handleDocumentClick (ev) {
    if (this.mounted) {
      if (!ReactDOM.findDOMNode(this).contains(ev.target)) {
        this.setState({ isOpen: false })
      }
    }
  }

  render () {
    const {
      className,
      style,
      disabled,
      inline,
      labelText,
      helpText,
      error,
      errorText,
    } = this.props;

    const classes = getClasses(className, disabled);
    const styles = getStyles(this.props, this.state);

    let labelTextElement = null;
    let helpTextElement = null;

    if (labelText) {
      labelTextElement = <span style={styles.label} className='label-text'>
        {labelText}
      </span>;
    }

    // error overrides helpText!
    if (error && errorText) {
      helpTextElement = <span style={styles.help} className='help-text error'>
        {errorText}
      </span>;
    }
    else if (helpText) {
      helpTextElement = <span style={styles.help} className='help-text'>
        {helpText}
      </span>;
    }

    const placeholderValue = (
        typeof this.state.selected === 'string' ||
        typeof this.state.selected === 'number'
      ) ? this.state.selected : this.state.selected.label;

    const value = <div className={classNames('placeholder', {'dirty': this.dirty})}>
      {placeholderValue}
    </div>;

    const menu = <div style={styles.menu} className={classNames('menu', {'open': this.state.isOpen})}>
      {this.buildMenu()}
    </div>;

    const icon = this.state.isOpen
      ? <ArrowUpIcon color={theme.colorBrandPrimary} />
      : <ArrowDownIcon color={theme.colorBrandPrimary} />;

    const controlProps = {
      style: styles.control,
      onMouseDown: this.handleMouseDown.bind(this),
      onTouchEnd: this.handleMouseDown.bind(this),
    };

    return (
      <div className={classes} style={Object.assign(styles.root, style)}>
        <div className='canister' style={styles.canister}>
          {labelTextElement}
          <div className='control' {...controlProps}>
            {value}
            <span className='arrow'>{icon}</span>
          </div>
          {helpTextElement}
        </div>
        {menu}
      </div>
    );
  }
}

export default Dropdown;
