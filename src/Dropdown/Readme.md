Dropdown allows you to select from a given set of options.

    const options = [
      'one', 'two', 'three'
    ];

    const groupOptions = [
      { value: 1, label: 'One' },
      { value: 2, label: 'Two' },
      {
        type: 'group', name: 'group1', items: [
          { value: 3, label: 'Three' },
          { value: 4, label: 'Four' }
        ]
      },
      {
        type: 'group', name: 'group2', items: [
          { value: 5, label: 'Five' },
          { value: 6, label: 'Six' }
        ]
      }
    ];

    <div style={{marginBottom: 280}}>
      <Dropdown labelText='Plain options' options={options} value='two' style={{marginRight: 9, display: 'inline-block'}} helpText='Do not leave empty'/>
      <Dropdown labelText='Grouped options' value={5} options={groupOptions} placeholder='Select grouped...' inline />
    </div>  
