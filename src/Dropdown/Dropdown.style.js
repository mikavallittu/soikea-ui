import colors from '../styles/colors';
import theme from '../styles/theme';
import transitions from '../styles/transitions';

const styles = {
  'dropdown': {
    fontFamily: theme.fontFamily,
    fontSize: theme.fontSizeBase,
    fontWeight: 400,
    position: 'relative',

    '& > .canister': {
      position: 'relative',
      width: 'auto',

      '& > .control': {
        position: 'relative',
        overflow: 'hidden',
        padding: '8px 12px',
        minWidth: 128,
        height: 36,
        boxSizing: 'border-box',
        border: '1px solid',
        borderColor: theme.colorBorder,
        borderRadius: 2,
        outline: 'none',
        backgroundColor: 'white',
        transition: 'ease 250ms border-color',

        '& > .placeholder': {
          color: colors.cloudy,
          marginRight: 12,

          '&.dirty': {
            color: theme.fontColor,
          }
        },

        '& > .arrow': {
          position: 'absolute',
          right: 6,
          top: 7
        },
      },

      '& > .label-text': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        fontSize: theme.fontSizeBase,
        lineHeight: theme.fontSizeBase + 'px',
        textAlign: 'left',
        paddingLeft: 1,
        transition: 'ease 250ms color',
        whiteSpace: 'nowrap',
        color: theme.fontColor,
        fontWeight: 600,
        marginTop: -24,
      },

      '& > .help-text': {
        position: 'absolute',
        left: 0,
        width: '100%',
        bottom: -20,
        fontSize: theme.fontSizeSmall,
        lineHeight: theme.fontSizeSmall + 'px',
        textAlign: 'left',
        paddingLeft: 1,
        transition: 'ease 250ms color',
        whiteSpace: 'nowrap',
        color: colors.cloudy,
        fontWeight: 500,

        '&.error': {
          color: theme.colorBrandError
        }
      },
    },

    '& > .menu': {
      backgroundColor: 'white',
      border: '1px solid',
      borderColor: theme.colorBorder,
      borderRadius: '0 0 2px 2px',
      boxShadow: '0 1px 0 ' + theme.colorShadow,
      boxSizing: 'border-box',
      marginTop: 2,
      padding: '5px 0',
      maxHeight: 312,
      overflowY: 'auto',
      position: 'absolute',
      top: '100%',
      width: '100%',
      zIndex: '1000',
      display: 'none',

      '&.open': {
        display: 'block',
      },

      '& .no-results': {
        color: colors.grayLight,
        padding: '6px 12px',
      },

      '& .option': {
        padding: '6px 12px',
        transition: 'ease 250ms background-color',

        '&:hover': {
          backgroundColor: theme.colorBackgroundLight,
        },

        '& > .check': {
          float: 'right',
          display: 'none',
          width: 20
        },

        '&.selected': {
          backgroundColor: theme.colorBrandPrimary,
          color: 'white',

          '& > .check': {
            display: 'inline-block'
          }
        }
      },

      '& .group': {
        padding: '6px 0',

        '& > header': {
          color: colors.grayLight,
          fontSize: '80%',
          textTransform: 'uppercase',
          padding: '4px 12px'
        }

      }
    },

    '&.is-disabled': {

      '& .control': {
        opacity: 0.6,
        cursor: 'not-allowed',
        backgroundColor: '#ECF1F7',
      }
    }
  }
};

export default styles;
