Input

    const styles = {
      backgroundColor: '#EAEFF2',
      padding: 24,
    };

    <div style={styles}>
      <Input labelText='Text input with placeholder' placeholder='Enter text' />

      <br />

      <Input labelText='Text input with default value' defaultValue='Piippukatu 11' />

      <br />

      <Input labelText='Disabled input' disabled />

      <br />

      <Input labelText='Read-only input' readOnly value="1123412" />

      <br />

      <Input labelText='Input with error' helpText='Kentässä saa olla vain isoja kirjaimia ja numeroita' defaultValue='underscore' error />
    </div>  
