import React from 'react';
import Colors from '../styles/colors';
import theme from '../styles/theme';
import { darken } from '../utils/colorManipulator';
import Transitions from '../styles/transitions';

function getClasses(className = '') {
  return 'input ' + className;
}

const inputSizes = {
  xs:   60,
  s:    120,
  m:    200,
  l:    320,
  full: '100%',
  auto: 'auto',
};

function getStyles(props, state) {
  let styles = {
    root: {
      width: inputSizes[props.size],
      display: props.inline ? 'inline-block' : 'block',
    },
    div: {
      position: 'relative',
      boxSizing: 'border-box',
      width: '100%',
      fontFamily: theme.fontFamily,
      transition: Transitions.easeOut('200ms', 'border-color'),
    },
    input: {
      display: 'inline-block',
      verticalAlign: 'middle',
      boxSizing: 'border-box',
      position: 'relative',
      width: '100%',
      //height: '100%',
      height: 36,
      padding: '9px 12px',
      border: '1px solid',
      borderColor: theme.colorBorder,
      borderRadius: 2,
      outline: 'none',
      backgroundColor: Colors.white,
      color: theme.fontColor,
      fontSize: theme.fontSizeBase,
      fontWeight: 400,
      transition: 'ease 250ms border-color',
    },
    disabled: {
      cursor: 'not-allowed',
      backgroundColor: '#ECF1F7',
      color: theme.fontColor,
      borderColor: theme.colorBorder,
    },
    readOnly: {
      backgroundColor: Colors.blackLight,
      color: Colors.grayLight,
    },
    error: {
      color: theme.colorBrandError,
      borderColor: theme.colorBrandError,
    },
    text: {
      position: 'absolute',
      left: 0,
      width: '100%',
      fontSize: theme.fontSizeBase,
      lineHeight: theme.fontSizeBase + 'px',
      paddingLeft: 1,
      transition: Transitions.easeOut(),
      whiteSpace: 'nowrap',

    },
    icon: {
      boxSizing: 'border-box',
      opacity: 0.35,
      position: 'absolute',
      left: 0,
      top: 0,
      width: '2.55rem',
      height: '2.55rem',
      marginLeft: '0.25rem',
      padding: '0.5rem',
    },
  };

  styles.div = Object.assign(
    styles.div,
    (props.error || props.required) && styles.error
  );

  if (props.labelText) {
    styles.div.marginTop = 36;
  }

  if (props.helpText || (props.error && props.errorText)) {
    styles.div.marginBottom = 28;
  }

  styles.helpText = Object.assign({}, styles.text, {
    color: (props.error || props.required)
      ? darken(theme.colorBrandError, 0.05)
      : '#aaa',
    fontWeight: 500,
    fontSize: theme.fontSizeSmall,
    top: 44,
    margin: 0,
  });

  styles.labelText = Object.assign({}, styles.text, {
    color: theme.fontColor,
    fontWeight: 600,
    top: -48,
    marginTop: 24,
  });

  if (state.isFocused && !props.readOnly) {
    styles.labelText.color = theme.colorBrandPrimary;
    styles.input.borderColor = theme.colorBrandPrimary;
  }

  if (props.error || props.required) {
    styles.input.color = styles.error.color;
    styles.input.borderColor = styles.error.borderColor;
  }

  if ((props.error || props.required) && state.isFocused) {
    styles.labelText.color = styles.error.color;
  }

  if (props.readOnly) {
    //styles.div.backgroundColor = styles.readOnly.backgroundColor;
    //styles.input.color = styles.readOnly.color;
    styles.input.opacity = 0.6;
    styles.input.backgroundColor = styles.disabled.backgroundColor;
    styles.input.borderColor = styles.disabled.borderColor;
    styles.input.color = styles.disabled.color;
  }

  if (props.disabled) {
    styles.input.opacity = 0.6;
    styles.input.backgroundColor = styles.disabled.backgroundColor;
    styles.input.borderColor = styles.disabled.borderColor;
    styles.input.color = styles.disabled.color;
    styles.input.cursor = styles.disabled.cursor;
  }

  return styles;
}


class Input extends React.Component {

  static propTypes = {
    /**
     * Set the error state
     */
    error: React.PropTypes.bool,

    /**
    * The error text to display at bottom. Shown only if error is `true`.
    */
    errorText: React.PropTypes.node,

    /**
    * The label text to display at top.
    */
    labelText: React.PropTypes.node,

    /**
    * The help text to display at bottom. Overridden by `errorText`.
    */
    helpText: React.PropTypes.node,

    /**
     * Make the input disabled.
     */
    disabled: React.PropTypes.bool,

    // readOnly: React.PropTypes.bool,

    // required: React.PropTypes.bool,

    /**
     * Callback function that is triggered when input's value changes. Function signature:
     * `(event, value, name) => {}`
     */
    onChange: React.PropTypes.func,

    // onBlur: React.PropTypes.func,

    // onFocus: React.PropTypes.func,

    /**
     * Set the value for the input.
     */
    value: React.PropTypes.any,

    /**
     * Name of the input. Passed as third parameter to `onChange` callback.
     */
    name: React.PropTypes.string,

    /**
     * Specifies the type of input to display such as "password" or "text".
     */
    type: React.PropTypes.string,

    /**
     * Override the inline-styles of the root div element.
     */
    style: React.PropTypes.object,

    /**
     * Override the inline-styles of the wrapped input element.
     */
    inputStyle: React.PropTypes.object,

    /**
     * Display as `inline-block` element
     */
    inline: React.PropTypes.bool,

    size: React.PropTypes.oneOf(['xs', 's', 'm', 'l', 'full', 'auto']),
  };

  state = {
    value: (this.props.value != undefined)
      ? this.props.value
      : (this.props.defaultValue != undefined)
        ? this.props.defaultValue
        : ''
  };

  static defaultProps = {
    inline: false,
    size: 'auto',
    type: 'text',
  };

  componentWillReceiveProps (newProps) {
    if (newProps.value != undefined) {
      this.setState({ value: newProps.value });
    }
  }

  render() {
    const {
      className,
      iconName,
      helpText,
      errorText,
      labelText,
      id,
      defaultValue,
      value,
      inline,
      error,
      onBlur,
      onChange,
      onFocus,
      style,
      inputStyle,
      ...other,
    } = this.props;

    const styles = getStyles(this.props, this.state);

    let helpTextElement = null;
    let labelTextElement = null;
    let iconElement = null;

    // error overrides helpText!
    if (error && errorText) {
      helpTextElement = (
        <span style={styles.helpText}>{errorText}</span>
      );
    }
    else if (helpText) {
      helpTextElement = (
        <span style={styles.helpText}>{helpText}</span>
      );
    }

    if (labelText) {
      labelTextElement = (
        <span style={styles.labelText}>{labelText}</span>
      );
    }

    // if (icon) {
    //   const icon = (typeof iconName === 'string')
    //     ? React.createElement(Icons[iconName] || 'span')
    //     : iconName;
    //
    //   iconElement = (
    //     <span style={styles.icon}>{icon}</span>
    //   );
    // }

    const inputProps = {
      id: this.props.id,
      style: Object.assign(styles.input, inputStyle),
      value: this.state.value,
      onChange: this._onChange.bind(this),
      onBlur: this._onBlur.bind(this),
      onFocus: this._onFocus.bind(this),
    };

    return (
      <div className={getClasses(className)} style={Object.assign(styles.root, style)}>
        <div style={styles.div}>
          {labelTextElement}
          {iconElement}
          <input {...other} {...inputProps} />
          {helpTextElement}
        </div>
      </div>
    );

  }

  focus() {
    this.input.focus();
  }

  getValue() {
    return this.state.value;
  }

  setValue(txt) {
    this.setState({value: txt});
  }

  clearValue() {
    this.setValue('');
  }

  _onChange(e) {
    const value = e.target.value;
    this.setState({value: value});
    if (this.props.onChange) this.props.onChange(e, value, this.props.name);
  }

  _onFocus(e) {
    this.setState({isFocused: true});
    if (this.props.onFocus) this.props.onFocus(e);
  }

  _onBlur(e) {
    this.setState({isFocused: false});
    if (this.props.onBlur) this.props.onBlur(e);
  }

}

export default Input;
