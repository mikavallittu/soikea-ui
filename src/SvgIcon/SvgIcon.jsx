import React from 'react';
import transitions from '../styles/transitions';

class SvgIcon extends React.Component {

  static propTypes = {
    /**
     * Elements passed into the SVG Icon.
     */
    children: React.PropTypes.node,

    /**
     * This is the fill color of the svg icon.
     */
    color: React.PropTypes.string,

    /**
     * The (size x size) of the icon (or string like 'auto' or 'inherit').
     */
    size: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),

    /**
     * This is the icon color when the mouse hovers over the icon.
     */
    hoverColor: React.PropTypes.string,

    /**
     * Function called when mouse enters this element.
     */
    onMouseEnter: React.PropTypes.func,

    /**
     * Function called when mouse leaves this element.
     */
    onMouseLeave: React.PropTypes.func,

    /**
     * Override the inline-styles of the root element.
     */
    style: React.PropTypes.object,

    /**
     * Allows you to redifine what the coordinates
     * without units mean inside an svg element. For example,
     * if the SVG element is 500 (width) by 200 (height), and you
     * pass viewBox="0 0 50 20", this means that the coordinates inside
     * the svg will go from the top left corner (0,0) to bottom right (50,20)
     * and each unit will be worth 10px.
     */
    viewBox: React.PropTypes.string,
  };

  static defaultProps = {
    viewBox: '0 0 24 24',
    size: 24,
  };

  state = {
    hovered: false,
  };

  handleMouseLeave = (event) => {
    this.setState({hovered: false});
    if (this.props.onMouseLeave) this.props.onMouseLeave(event);
  };

  handleMouseEnter = (event) => {
    this.setState({hovered: true});
    if (this.props.onMouseEnter) this.props.onMouseEnter(event);
  };

  render() {
    const {
      children,
      color,
      hoverColor,
      onMouseEnter,
      onMouseLeave,
      style,
      size,
      viewBox,
      ...other,
    } = this.props;

    const offColor = color ? color :
      style && style.fill ? style.fill : 'black';

    const onColor = hoverColor ? hoverColor : offColor;

    const mergedStyles = Object.assign({
      display: 'inline-block',
      fill: this.state.hovered ? onColor : offColor,
      height: size,
      width: size,
      userSelect: 'none',
      transition: transitions.easeOut('250ms', 'all'),
    }, style);

    return (
      <svg
        {...other}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        style={mergedStyles}
        viewBox={viewBox}
      >
        {children}
      </svg>
    );
  }
}

export default SvgIcon;
