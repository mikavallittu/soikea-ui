Checkbox select multiple options from a list.

    <div>
      <Checkbox labelText='Checkbox 1' checked />

      <hr />

      <Checkbox labelText='Checkbox 2' />

      <hr />

      <Checkbox labelText='Disabled checkbox' disabled />

      <hr />

      <Checkbox labelText='Big checkbox' size={24} />
    </div>  
