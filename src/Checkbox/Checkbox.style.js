import Colors from '../styles/colors';
import Theme from '../styles/theme';

const defaultSize = 16;

// mixin
const flexCenter = {
  display: 'inline-flex',
  justifyContent: 'center',
  alignItems: 'center',
};

const styles = {
  'checkbox': {
    ...flexCenter,

    '& > .box': {
      width: defaultSize,
      height: defaultSize,
      backgroundColor: 'white',
      borderColor: Theme.colorBorder,
      borderStyle: 'solid',
      borderRadius: 3,
      borderWidth: 1,
      boxSizing: 'border-box',
      padding: 0,
      cursor: 'pointer',
      boxShadow: 'none',
      ...flexCenter,

      '& > svg': {
        //visibility: 'hidden',
        opacity: 0,
        fill: Colors.grayDark,
        width: defaultSize / 2,
        height: defaultSize / 2,
      },
    },

    '& > .label': {
      marginLeft: 9,
      fontSize: 15,
      fontWeight: 400,
      fontFamily: Theme.fontFamily,
      verticalAlign: 'middle',
    },
  },
};

export default styles;
