import React from 'react';
import classNames from 'classnames';
import CheckIcon from './check-icon';
import createStyleSheet from '../utils/createStyleSheet';
import styles from './Checkbox.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className){
  const classes = styleSheet.classes;
  return classNames(classes.checkbox || 'checkbox', className);
}

function getStyles(props, state) {
  const { size } = props;

  const styles = {
    box:  {},
    icon: {
      width: size / 2,
      height: size / 2,
    }
  };

  if (size !== 16) {
    styles.box = {
      width: size,
      height: size,
    }
  }

  if (props.disabled) {
    styles.box.opacity = 0.5;
    styles.box.backgroundColor = '#eee';
    styles.box.cursor = 'default';
  }

  if (state.checked) {
    //styles.icon.visibility = 'visible';
    styles.icon.opacity = 1;
  }

  return styles;
}


class Checkbox extends React.Component {

  static propTypes = {
    /**
     * Set checked state for the checkbox.
     */
    checked: React.PropTypes.bool,

    /**
     * Make the checkbox disabled.
     */
    disabled: React.PropTypes.bool,

    /**
     * This is the size of the checkbox in pixels.
     */
    size: React.PropTypes.number,

    /**
     * The label text to show next to the checkbox.
     */
    labelText: React.PropTypes.node,

    /**
     * alias for `labelText`.
     */
    label: React.PropTypes.node,

    /**
     * Overrides the inline-styles of the icon element.
     */
    iconStyle: React.PropTypes.object,

    /**
     * Overrides the inline-styles of the checkbox element label.
     */
    labelStyle: React.PropTypes.object,

    /**
     * Callback function that is triggered when checkbox is clicked. Function signature:
     * <code>
     *    (event, checked, name) => {}
     * </code>
     */
    onChange: React.PropTypes.func,

    /**
     * Name of the checkbox. Passed as third parameter to `onChange` callback.
     */
    name: React.PropTypes.string,
  };

  static defaultProps = {
    checked: false,
    disabled: false,
    size: 16,
  };

  state = {
    checked: this.props.checked,
  };

  componentWillReceiveProps(nextProps) {
    // Note: this method is not called for the initial render!
    //
    // We use it for the incoming `checked` property to cause a
    // state change.
    this.setChecked(nextProps.checked);
  }

  render() {
    let {
      className,
      size,
      style,
      label,
      labelText,
      labelStyle,
      iconStyle,
      ...other,
    } = this.props;

    const styles = getStyles(this.props, this.state);
    const cls = getClasses();
    const boxStyles = styles.box;
    iconStyle = Object.assign(styles.icon, iconStyle);

    let labelTextElement = null;
    // NOTE: property `labelText` precedes over `label`
    labelText = labelText || label;

    if (labelText) {
      labelTextElement = (
        <span className='label' style={labelStyle}>{labelText}</span>
      );
    }

    return (
      <div {...other} className={cls} style={style}>
        <span className='box' style={boxStyles} onClick={this._onClick.bind(this)}>
          <CheckIcon style={iconStyle} />
        </span>
        {labelTextElement}
      </div>
    );
  }

  isChecked() {
    return this.state.value;
  }

  setChecked(state) {
    this.setState({ checked: Boolean(state) });
  }

  _onClick(e) {
    if (this.props.disabled) return;
    const checked = !this.state.checked;
    this.setState({ checked });
    if (this.props.onChange) this.props.onChange(e, checked, this.props.name);
  }

}

export default Checkbox;
