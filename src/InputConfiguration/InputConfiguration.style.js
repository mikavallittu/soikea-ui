import colors from '../styles/colors';
import theme from '../styles/theme';

const styles = {
  'input-configuration': {
    padding: '65px 20px 35px 20px',
    position: 'relative',

    '& > .header': {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      padding: '15px 20px',
      color: 'white',
      fontFamily: theme.fontFamily,
      fontWeight: '500',
      background: theme.colorBrandPrimary,
      borderRadius: '3px 3px 0 0',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',

      '& > h3': {
        display: 'inline-block',
        margin: 0,
        color: 'white',
        fontSize: '16px',
        fontWeight: '400',
        lineHeight: '1.3em'
      },

      '& > .onoff': {
        
      }
    },

    '& .group': {
      paddingTop: 10,

      '&:first-child': {
        paddingRight: 20,
      }
    },

    '& > .section': {
      display: 'flex',

      '& > .col': {
        flex: 1,
      },
    },

    '& .input-switch': {
      paddingTop: 5,

      '& > .label': {
        display: 'inline-block',
        maxWidth: '100%',
        marginBottom: '6px',
        fontSize: '14px',
        fontWeight: '600',
      },

      '& > .options': {
        width: '150px',
        overflow: 'hidden',
        padding: 0,
        textAlign: 'center',
        borderRadius: 3,
        border: '1px solid #e5e8ed',
        display: 'flex',

        '&.disabled': {
          opacity: 0.6,
        },

        '& > .option': {
          display: 'block',
          float: 'left',
          width: '50%',
          cursor: 'pointer',
          padding: '8px 6px 9px 6px',
          fontSize: '14px',
          lineHeight: '20px',

          '&.selected': {
            color: 'white',
            background: theme.colorBrandPrimary
          }
        },

        '&.disabled > .option': {
          cursor: 'not-allowed',
        }
      },
    },

  }
};

export default styles;
