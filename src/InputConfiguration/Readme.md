InputConfiguration

    const styles = {backgroundColor: '#EAEFF2', padding: 24};

    const alertTypes = [
      '1. kiireellisyysluokan hälytys',
      '2. kiireellisyysluokan hälytys',
      '3. kiireellisyysluokan hälytys',
    ];

    const value = {
      id: 1,
      active: true,
      alertType: '1. kiireellisyysluokan hälytys',
      alertName: 'Palo 2',
      alertTime: 5,
      alertState: 'open',
      alertOutputAddress: ''
    };

    <div style={styles}>
      <InputConfiguration alertTypes={alertTypes} value={value} />
    </div>  
