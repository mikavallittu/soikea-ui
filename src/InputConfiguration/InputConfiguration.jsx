import React, { Component } from 'react';
import classNames from 'classnames';
import createStyleSheet from '../utils/createStyleSheet';
import styles from './InputConfiguration.style';
import Paper from '../Paper';
import Input from '../Input';
import Dropdown from '../Dropdown';
import Toggle from '../Toggle';

const styleSheet = createStyleSheet(styles);

const defaultMessages = {
  alertType: 'Hälytyksen tyyppi',
  alertName: 'Hälytyksen nimi',
  alertTime: 'Hälytysaika minuutteina',
  alertState: 'Hälytystila',
  alertOutputAddress: 'Meno-osoite',
  open: 'Auki',
  closed: 'Kiinni',
  on: 'On',
  off: 'Off',
};

function getClasses(className) {
  const classes = styleSheet.classes;
  return classNames(classes['input-configuration'] || 'input-configuration', className);
}

function InputSwitch(props) {
  const {
    name,
    labelText,
    disabled,
    open,
    onClick,
  } = props;

  const messages = props.messages || {};

  return (
    <div className="input-switch">
      <label className='label'>
        {labelText}
      </label>
      <div className={classNames('options', {'disabled': disabled})}>
        <div
          onClick={onClick.bind('open', name)}
          className={classNames('option', {'selected': open})}>
          {messages.open}
        </div>
        <div
          onClick={onClick.bind('closed', name)}
          className={classNames('option', {'selected': !open})}>
          {messages.closed}
        </div>
      </div>
    </div>
  );
}

class InputConfiguration extends Component {

  static propTypes = {
    /**
    * Value expects the following object shape:
    * <pre>
    *   {
    *     id: Number,
    *     active: Boolean,
    *     alertType: String,
    *     alertName: String,
    *     alertTime: Number,
    *     alertState: String,
    *     alertOutputAddress: String,
    *   }
    * </pre>
    */
    value: React.PropTypes.object.isRequired,

    onChange: React.PropTypes.func.isRequired,

    /**
     * Make the input configuration disabled.
     */
    disabled: React.PropTypes.bool,

    /**
    * Dropdown options for alert type.
    */
    alertTypes: React.PropTypes.array,

    /**
     * Translations object for input labels. Defaults:
     * <pre>
     *   {
     *     alertType: 'Hälytyksen tyyppi',
     *     alertName: 'Hälytyksen nimi',
     *     alertTime: 'Hälytysaika minuutteina',
     *     alertState: 'Hälytystila',
     *     alertOutputAddress: 'Meno-osoite',
     *   }
     * </pre>
     */
    messages: React.PropTypes.object,
  };

  static defaultProps = {
    disabled: true,
    alertTypes: []
  };

  state = {
    disabled: this.props.disabled,
  };

  componentWillReceiveProps(nextProps) {
    // Note: this method is not called for the initial render. We use it for the
    // incoming `disabled` property to cause a state change.
    this.setDisabled(nextProps.disabled);
  }

  setDisabled(state) {
    this.setState({ disabled: Boolean(state) });
  }

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.onToggle = this.onToggle.bind(this);
  }

  onChange(event, value, name) {
    if (this.props.onChange) {
      this.props.onChange(Object.assign({}, this.props.value, {[name]: value}));
    }
  }

  onToggle(event, isSwitched, name) {
    this.setState({
      disabled: !isSwitched
    });
    this.onChange(event, isSwitched, 'active');
  }

  render() {
    const {
      alertTypes,
      value,
      className,
      style,
      ...other,
    } = this.props;

    const classes = getClasses(className);
    const disabled = this.state.disabled;
    const messages = this.props.messages || defaultMessages;

    return (
      <Paper className={classes}>
        <div className="header">
          <h3>#{value.id}</h3>
          <div className="onoff">
            <Toggle
              labelText={!disabled ? messages.on : messages.off}
              labelPosition='left'
              onToggle={this.onToggle}
              toggled={!disabled}
            />
          </div>
        </div>

        <div className="group">
          <Dropdown
            name='alertType'
            labelText={messages.alertType}
            options={alertTypes}
            value={value.alertType}
            onChange={this.onChange}
            disabled={disabled} />
        </div>

        <div className="group">
          <Input
            name='alertName'
            labelText={messages.alertName}
            value={value.alertName}
            onChange={this.onChange}
            disabled={disabled} />
        </div>

        <div className="section">
          <div className="col group">
            <Input
              name='alertTime'
              labelText={messages.alertTime}
              value={value.alertTime}
              onChange={this.onChange}
              disabled={disabled} />
          </div>
          <div className="col group">
            <InputSwitch
              name='alertState'
              labelText={messages.alertState}
              messages={messages}
              disabled={disabled}
              onClick={this.onChange}
              open={value.alertState === 'open'} />
          </div>
        </div>

        <div className="group">
          <Input
            name='alertAddress'
            labelText='Meno-osoite'
            placeholder='Valitse'
            onChange={this.onChange}
            disabled={disabled} />
        </div>
      </Paper>
    )
  }
}

export default InputConfiguration;
