import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete';
//import Autocomplete from './autocomplete';
import Colors from '../styles/colors';
import theme from '../styles/theme';
import { darken } from '../utils/colorManipulator';
import Transitions from '../styles/transitions';
import CloseIcon from '../svg-icons/close';
import AddIcon from '../svg-icons/add';
import ArrowDownIcon from '../svg-icons/arrow-down';

function getStyles(props, state) {
  const styles = {
    root: {
      display: props.inline ? 'inline-block' : 'block',
      width: 512,
    },
    div: {
      position: 'relative',
      boxSizing: 'border-box',
      width: '100%',
      fontFamily: theme.fontFamily,
      transition: Transitions.easeOut('200ms', 'border-color'),
    },
    input: {
      display: 'inline-block',
      verticalAlign: 'middle',
      boxSizing: 'border-box',
      position: 'relative',
      width: '100%',
      //width: 312,
      height: 36,
      padding: '9px 32px 9px 12px',
      border: '1px solid',
      borderColor: theme.colorBorder,
      borderRadius: 2,
      outline: 'none',
      backgroundColor: Colors.white,
      color: theme.fontColor,
      fontSize: theme.fontSizeBase,
      fontWeight: 400,
      fontFamily: theme.fontFamily,
      transition: 'ease 250ms border-color',
    },
    disabled: {
      cursor: 'not-allowed',
      backgroundColor: '#ECF1F7',
      color: theme.fontColor,
      borderColor: theme.colorBorder,
    },
    readOnly: {
      backgroundColor: Colors.blackLight,
      color: Colors.grayLight,
    },
    text: {
      position: 'absolute',
      left: 0,
      width: '100%',
      fontSize: theme.fontSizeBase,
      lineHeight: theme.fontSizeBase + 'px',
      paddingLeft: 1,
      transition: Transitions.easeOut(),
      whiteSpace: 'nowrap',
    },
    icon: {
      position: 'absolute',
      marginLeft: -28,
      top: 6,
      //fill: Colors.cloudy,
      fill: theme.colorBrandPrimary,
      cursor: 'pointer',
    }
  };

  if (props.labelText) {
    styles.div.marginTop = 36;
  }

  styles.labelText = Object.assign({}, styles.text, {
    color: theme.fontColor,
    fontWeight: 600,
    top: -48,
    marginTop: 24,
  });

  if (props.disabled) {
    styles.input.opacity = 0.6;
    styles.icon.opacity = 0.6;
    styles.input.backgroundColor = styles.disabled.backgroundColor;
    styles.input.borderColor = styles.disabled.borderColor;
    styles.input.color = styles.disabled.color;
    styles.input.cursor = styles.disabled.cursor;
    styles.icon.cursor = styles.disabled.cursor;
  }

  return styles;
}

function renderMenu(items, value, style) {
  style.left = 0;
  return <div style={{...style, ...this.menuStyle}} children={items} />;
}

function renderItem(item, isHighlighted) {
  const styles = {
    item: {
      padding: '6px 12px',
    },
    highlightedItem: {
      backgroundColor: theme.colorBackgroundLight,
    },
    tag: {
      display: 'inline-block',
      backgroundColor: theme.colorBrandPrimary,
      borderRadius: 2,
      margin: '0 10px 0 5px',
      padding: '0 5px',
      color: 'white',
      fontWeight: 200,
      minWidth: 90,
      textAlign: 'center',
    }
  };

  if (this.tagStyle) {
    styles.tag = Object.assign(styles.tag, this.tagStyle);
  }

  if (item.tag && item.tag.color) {
    styles.tag.backgroundColor = item.tag.color;
  }

  let tagElement = null;
  if (item.tag && item.tag.label) {
    tagElement = <span style={styles.tag}>{item.tag.label}</span>;
  }

  if (this.useValueAsTag) {
    tagElement = <span style={styles.tag}>{item.value}</span>;
  }

  return (
    <div
      style={Object.assign(styles.item, isHighlighted ? styles.highlightedItem : {})}
      key={item.value}>
      {tagElement}
      {item.name}
    </div>
  );
}

class AutoComplete extends Component {
  static propTypes = {
    /**
     * The label text to display at top.
     */
    labelText: React.PropTypes.node,

    /**
     * Invoked when close icon is clicked.
     */
    onClear: React.PropTypes.func,
  };

  static defaultProps = {
    onClear () {},
    inputProps: {},
    menuStyle: {
      backgroundColor: 'white',
      border: '1px solid',
      borderColor: theme.colorBorder,
      borderRadius: '0 0 2px 2px',
      boxShadow: '0 1px 0 ' + theme.colorShadow,
      boxSizing: 'border-box',
      marginTop: 2,
      padding: '5px 0',
      maxHeight: 310,
      overflowY: 'auto',
      position: 'absolute',
      top: '100%',
      width: '100%',
      zIndex: '1000',
    },
    renderMenu,
    renderItem,
  };

  render() {
    const {
      style,
      labelText,
      inputProps,
      onClear,
      disabled,
      ...other
    } = this.props;

    const styles = getStyles(this.props, this.state);

    const _inputProps = Object.assign({}, inputProps, {
      disabled,
      style: Object.assign(styles.input, inputProps.style),
    });

    let labelTextElement = null;
    if (labelText) {
      labelTextElement = <span style={styles.labelText}>{labelText}</span>;
    }

    const SvgIcon = this.props.buttonType === 'add' ? AddIcon : ArrowDownIcon;

    let icon = <SvgIcon style={styles.icon} onClick={() => {
        if (!disabled) {
          this.refs.autocomplete.refs.input.focus();
        }
      }}/>;

    if (this.props.value != null && this.props.value !== '') {
      icon = <SvgIcon style={styles.icon} onClick={() => {
          if (!disabled) {
            onClear();
            // XXX: focus can break if 'react-autocomplete' internals change
            this.refs.autocomplete.handleInputFocus();
            this.refs.autocomplete.refs.input.focus();
          }
        }} />;
    }


    const autocompleteProps = {
      ref: 'autocomplete',
      inputProps: _inputProps,
      wrapperStyle: {display: 'inline-block', width: '100%'},
      ...other
    };

    return (
      <div className='autocomplete' style={Object.assign(styles.root, style)}>
        <div style={styles.div}>
          {labelTextElement}
          <Autocomplete {...autocompleteProps} />
          {icon}
        </div>
      </div>
    );
  }
}

export default AutoComplete;
