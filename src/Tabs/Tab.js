import React from 'react';

const Tab = (props) => {
  return (
    <div className='tab'>
      {props.children}
    </div>
  );
};

Tab.propTypes = {
  badge: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number,
  ]),
  children: React.PropTypes.element.isRequired,
  href: React.PropTypes.string,
  label: React.PropTypes.string.isRequired,
};

export default Tab;
