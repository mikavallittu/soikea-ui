import React, {
  Component,
  Children,
  createElement,
  isValidElement,
} from 'react';
import classNames from 'classnames';
import createStyleSheet from '../utils/createStyleSheet';
import styles from './Tabs.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className){
  const classes = styleSheet.classes;
  return classNames(classes.tabs || 'tabs', className);
}

class Tabs extends Component {

  static propTypes = {
    /**
     * Index of the active tab.
     */
    activeTab: React.PropTypes.number,

    /**
     * Use only <Tab /> elements as children.
     */
    children: React.PropTypes.node,
  };

  static defaultProps = {
    activeTab: 0,
  };

  state = {
    activeTab: this.props.activeTab,
  };

  componentWillReceiveProps(nextProps) {
    this.setActive(nextProps.activeTab);
  }

  setActive(index) {
    this.setState({ activeTab: index });
  }

  getTabs(props = this.props) {
    const tabs = [];

    Children.forEach(props.children, (tab) => {
      if (isValidElement(tab)) {
        tabs.push(tab);
      }
    });

    return tabs;
  }

  getTabClassName(index) {
    return classNames({'active': index === this.state.activeTab});
  }

  render() {
    const {
      className,
    } = this.props;

    const classes = getClasses(className);
    const tabs = this.getTabs();

    const tab = tabs[this.state.activeTab];
    let tabElement = null;

    // NOTE: for the require to work in the Readme.md example we need to
    // 'export default' by hand
    if (tab) {
      tabElement = createElement(
        typeof tab.type === 'object' ? tab.type.default : tab.type,
        tab.props
      );
    }

    const tabLabels = tabs.map((child, index) => {
      let badgeElement = null;
      if (child.props.badge) {
        badgeElement = <span className='badge'>{child.props.badge}</span>;
      }
      return (
        <li
          key={index}
          className={this.getTabClassName(index)}
          onClick={this.handleClick.bind(this, index)}>
          <a href={child.props.href}>{child.props.label}</a>
          {badgeElement}
        </li>
      );
    });

    return (
      <div className={classes}>
        <div className='labels'>
          <ul>
            {tabLabels}
          </ul>
        </div>

        <div className='content'>
          {tabElement}
        </div>
      </div>
    );
  }

  handleClick(index) {
    this.setActive(index);
  }

}

export default Tabs;
