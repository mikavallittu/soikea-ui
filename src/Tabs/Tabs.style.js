import colors from '../styles/colors';
import theme from '../styles/theme';

const styles = {
  'tabs': {
    '& > .labels': {
      backgroundColor: 'white',
      borderBottom: '2px solid ' + theme.colorBorder,

      '& > ul': {
        margin: 0,
        padding: 0,

        fontFamily: theme.fontFamily,
        fontSize: theme.fontSizeLarge,
        fontWeight: 300,

        '& > li': {
          top: '2px',
          position: 'relative',
          listStyle: 'none',
          display: 'inline-block',
          lineHeight: '68px',
          cursor: 'pointer',

          '& > a': {
            color: colors.cloudy,
            textDecoration: 'none',
            margin: '0 20px',
          },

          '&.active': {
            borderColor: theme.colorBrandPrimary,
            borderStyle: 'solid',
            borderWidth: '0 0 2px 0',

            '& > a': {
              color: theme.colorBrandPrimary,
            },
          },

          '& > .badge': {
            position: 'absolute',
            display: 'inline-flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.red,
            top: '6px',
            right: '0px',
            height: '20px',
            width: '20px',
            borderRadius: '10px',
            fontSize: '13px',
            fontWeight: '500',
            color: 'white'
          },
        }
      }
    },

    '& > .content': {
      margin: '35px 25px'
    },
  }
};

export default styles;
