Tabs

    const Tab = require('./Tab');

    <Tabs activeTab={2}>
      <Tab label="Tab 1">
        <div>My tab 1</div>
      </Tab>
      <Tab label="Tab 2">
        <div>My tab 2</div>
      </Tab>
      <Tab label="Tab 3">
        <div>My tab 3</div>
      </Tab>
    </Tabs>
