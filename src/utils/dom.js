export default {

  isDescendant(parent, child) {
    let node = child.parentNode;

    while (node !== null) {
      if (node === parent) return true;
      node = node.parentNode;
    }

    return false;
  },

  hasParent(child, className, travelUp = 3) {
    let level = 0;
    let node = child.parentNode;

    if (child.className === className) return true;

    while (node !== null && level < travelUp) {
      if (node.className === className) return true;
      node = node.parentNode;
      level++;
    }

    return false;
  },

  offset(el) {
    const rect = el.getBoundingClientRect();
    return {
      top: rect.top + document.body.scrollTop,
      left: rect.left + document.body.scrollLeft,
    };
  },

};
