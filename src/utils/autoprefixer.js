import InlineStylePrefixer from 'inline-style-prefixer';

export default function(userAgent) {
  if (userAgent === false) { // Disabled autoprefixer
    return null;
  } else if (userAgent === 'all' || userAgent === undefined) { // Prefix for all user agent
    return (style) => InlineStylePrefixer.prefixAll(style);
  } else {
    const prefixer = new InlineStylePrefixer({
      userAgent: userAgent,
    });
    return (style) => prefixer.prefix(style);
  }
}
