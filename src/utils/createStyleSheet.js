import jss from 'jss'
import camelCase from 'jss-camel-case'
import defaultUnit from 'jss-default-unit'
import nested from 'jss-nested'

// Use JSS plugins
jss.use(camelCase());
jss.use(defaultUnit());
jss.use(nested());

export default function(style, debug = false) {
  const sheet = jss.createStyleSheet(style, {
    named: true
  });
  if (debug) {
    console.log(sheet.toString());
  }
  // Insert style into DOM
  sheet.attach();
  return sheet;
}
