import Colors from '../styles/colors';
import Theme from '../styles/theme';

const styles = {
  'loader-box': {
    display: 'block',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 10,
    transition: 'ease 250ms background-color',
    backgroundColor: 'rgba(0,0,0,0)',
    visibility: 'hidden',

    '&.visible': {
      backgroundColor: 'rgba(0,0,0,0.12)',
      visibility: 'visible',
    },

    '& > div': {
      display: 'table',
      width: '100%',
      height: '100%',
      textAlign: 'center',
      zIndex: 20,
      color: 'white',

      '& > div': {
        display: 'table-cell',
        verticalAlign: 'middle',
      }
    }
  }
};

export default styles;
