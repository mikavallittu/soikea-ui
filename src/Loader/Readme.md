Loader component is used to show an indicator that something is happening .

    initialState = {
      isLoading: false,
      data: []
    };

    function loadData(){
      setState({ isLoading: true });

      setTimeout(function(){
        setState({
          isLoading: false,
          data: [
            {name: 'Apple', color: 'Red', desc: 'These are red'}
          ]
        })
      }, 2000);
    }

    <div>
      <Button onClick={loadData}>Load table data</Button>

      <Loader show={state.isLoading} showDelay={500}>
        <table style={{margin: '9px 0'}}>
          <thead>
            <tr>
              <th>Name</th>
              <th>Color</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {state.data.map((item, i) => (
              <tr key={i}>
                <td>{item.name}</td>
                <td>{item.color}</td>
                <td>{item.desc}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </Loader>
    </div>
