import React, { Component } from 'react';
import { EventEmitter } from 'events';
import LoaderBox from './LoaderBox';

function uid() {
  return Math.random().toString(36).substr(2, 9);
}

const loaderStack = {
  ...EventEmitter.prototype,

  stack: [],

  addLoader(id, priority = 0) {
    if (this.getIndex(id) === -1) {
      this.stack.push({ id, priority });
      this.emitChange();
    }
  },

  removeLoader(id) {
    if (this.getIndex(id) !== -1) {
      this.stack.splice(this.getIndex(id), 1);
      this.emitChange();
    }
  },

  getIndex(id) {
    return this.stack.findIndex(loader => loader.id === id);
  },

  getMaxPriority() {
    let max = 0;

    for (const value of this.stack) {
      if (value.priority > max) {
        max = value.priority;
      }
    }

    return max;
  },

  emitChange() {
    this.emit('change');
  },

  addChangeListener(callback) {
    this.on('change', callback);
  },

  removeChangeListener(callback) {
    this.removeListener('change', callback);
  },
};

const Loader = React.createClass({
  propTypes: {
    /**
     * Show (or hide) the loader.
     */
    show: React.PropTypes.bool.isRequired,

    /**
     * Children passed into the loader element.
     */
    children: React.PropTypes.node,

    /**
     * Define the inline-styles of the loader background shadow.
     */
    backgroundStyle: React.PropTypes.object,

    /**
     * Message string or spinner element.
     */
    message: React.PropTypes.node,

    /**
     * Stack priority.
     */
    priority: React.PropTypes.number,

    /**
     * Delay the display of the loader (in milliseconds)
     */
    showDelay: React.PropTypes.number,
  },

  getDefaultProps() {
    return {
      priority: 0,
      message: 'Loading...'
    };
  },

  getInitialState() {
    return {
      active: false
    };
  },

  componentWillMount() {
    this._stackId = uid();
  },

  componentDidMount() {
    loaderStack.addChangeListener(this.onStackChange);
    this.initialize(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.initialize(nextProps);
  },

  componentWillUnmount() {
    loaderStack.removeChangeListener(this.onStackChange);
  },

  onStackChange() {
    if (this.isMounted()) {
      this.setState({
        active: loaderStack.getMaxPriority() === this.props.priority,
      });
    }
  },

  initialize(props) {
    if (props.show) {
      loaderStack.addLoader(this._stackId, props.priority);
    } else {
      loaderStack.removeLoader(this._stackId);
    }
  },

  render() {
    const {
      children,
      contentBlur,
      hideContentOnLoad,
      backgroundStyle,
      message,
      show,
      showDelay,
    } = this.props;

    const {
      active,
    } = this.state;

    const shouldShowLoader = active && show;

    return (
      <div style={{position: 'relative'}}>
        <div>
          {children}
        </div>

        {shouldShowLoader && (
          <LoaderBox delay={showDelay} style={backgroundStyle}>
            {message}
          </LoaderBox>
        )}
      </div>
    );
  },
});

export default Loader;
