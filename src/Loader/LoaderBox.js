import React, { Component } from 'react';
import classNames from 'classnames';
import createStyleSheet from '../utils/createStyleSheet';
import styles from './Loader.style';

const styleSheet = createStyleSheet(styles);

function getClasses(){
  return styleSheet.classes['loader-box'];
}

class LoaderBox extends Component {

  static propTypes = {
    delay: React.PropTypes.number,
  };

  static defaultProps = {
    delay: 0,
  };

  constructor() {
    super();
    this.state = {
      visible: false,
    };
  }

  componentWillMount() {
    this._timeout = setTimeout(() => {
      this.setState({ visible: true });
    }, this.props.delay);
  }

  componentWillUnmount() {
    this._timeout && clearTimeout(this._timeout);
  }

  render () {
    let rootClass = getClasses('loader-box');

    if (this.state.visible) {
      rootClass += ' visible';
    }

    return (
      <div style={this.props.style} className={rootClass}>
        <div>
          <div>
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default LoaderBox;
