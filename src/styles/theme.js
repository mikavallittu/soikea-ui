import colors from './colors';
import { lighten, darken } from '../utils/colorManipulator';

export default {
  fontFamily: '"Raleway", "Helvetica Neue", Helvetica, Arial, sans-serif',

  colorBrandPrimary:    '#3C9ADF',
  colorBrandSuccess:    '#48E47D',
  colorBrandError:      '#E94653',
  colorBrandWarning:    '#E9C841',

  colorBackgroundDark:  '#323A46',
  colorBackgroundLight: '#EAEFF2',
  colorShadow:          '#DADFE2',
  colorBorder:          '#D5DADF',

  fontColor:            '#323A46',
  fontColorLight:       colors.grayDark,

  fontWeightLight:      300,
  fontWeightNormal:     400,
  fontWeightBold:       600,

  fontSizeBase:         14,
  fontSizeSmall:        12,
  fontSizeLarge:        18,

  borderRadiusBase:     4,
  borderRadiusSmall:    2,
  borderRadiusLarge:    9,
}
