export default {
  iconSize: 24,
  gutter: 20,
  gutterMini: 8,
  gutterLess: 16,
  gutterWide: 48,
  toolbarHeight: 56,
};
