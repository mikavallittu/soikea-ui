/**
 * COLOR VARIABLES
 *
 *  http://clrs.cc
 */

export default {
  aqua:    '#7FDBFF',
  blue:    '#0074D9',
  navy:    '#001F3F',
  teal:    '#39CCCC',
  green:   '#2ECC40',
  olive:   '#3D9970',
  lime:    '#01FF70',

  yellow:  '#FFDC00',
  orange:  '#FF851B',
  red:     '#FF4136',
  fuchsia: '#F012BE',
  purple:  '#B10DC9',
  maroon:  '#85144B',

  grayDarker:  '#222',
  grayDark:    '#333',
  gray:        '#555',
  grayLight:   '#777',
  grayLighter: '#EEE',

  cloudy:      '#AAA',
  silver:      '#DDD',
  black:       '#000',
  dark:        '#111',
  white:       '#FFF',

  transparent: 'rgba(0, 0, 0, 0)',
  blackFull:   'rgba(0, 0, 0, 1)',
  whiteFull:   'rgba(255, 255, 255, 1)',
  blackDark:   'rgba(0, 0, 0, 0.87)',
  blackLight:  'rgba(0, 0, 0, 0.54)',
  blackMin:    'rgba(0, 0, 0, 0.26)',
  blackFaint:  'rgba(0, 0, 0, 0.12)',
  whiteDark:   'rgba(255, 255, 255, 0.87)',
  whiteLight:  'rgba(255, 255, 255, 0.54)'
}
