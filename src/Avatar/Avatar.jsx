import React from 'react';
import classNames from 'classnames';
import createStyleSheet from '../utils/createStyleSheet';
import styles from './Avatar.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className, squared){
  const classes = styleSheet.classes;

  return classNames(classes.avatar || 'avatar', {
    squared
  }, className);
}

function getStyles(props) {
  const {
    backgroundColor,
    color,
    size,
    src,
  } = props;

  const styles = {
    root: {
      lineHeight: `${size}px`,
      fontSize: size / 2 - 4,
      height: size,
      width: size,
    }
  };

  if (backgroundColor) {
    styles.root.backgroundColor = backgroundColor;
  }
  if (color) {
    styles.root.color = color;
  }

  if (src) {
    Object.assign(styles.root, {
      background: `url(${src})`,
      backgroundSize: size,
      backgroundOrigin: 'border-box',
      backgroundColor: 'transparent',
      height: size - 2,
      width: size - 2,
    });
  }

  return styles;
}

class Avatar extends React.Component {
  static propTypes = {
    /**
     * The backgroundColor of the avatar. Does not apply to image avatars.
     */
    backgroundColor: React.PropTypes.string,

    /**
     * Can be used, for instance, to render a letter inside the avatar.
     */
    children: React.PropTypes.node,

    /**
     * The css class name of the root `div` element.
     */
    className: React.PropTypes.string,

    /**
     * The icon or letter's color.
     */
    color: React.PropTypes.string,

    /**
     * This is the SvgIcon to be used inside the avatar.
     */
    icon: React.PropTypes.element,

    /**
     * This is the size of the avatar in pixels.
     */
    size: React.PropTypes.number,

    /**
     * Use square shape for the avatar.
     */
    squared: React.PropTypes.bool,

    /**
     * If passed in, this component will render an image backgound.
     */
    src: React.PropTypes.string,

    /**
     * Override the inline-styles of the root element.
     */
    style: React.PropTypes.object,
  };

  static defaultProps = {
    squared: false,
    size: 40,
  };

  render() {
    const {
      icon,
      color,
      size,
      squared,
      src,
      style,
      className,
      ...other,
    } = this.props;

    const styles = getStyles(this.props);
    const classes = getClasses(className, squared);

    if (src) {
      return (
        <div
          {...other}
          style={Object.assign(styles.root, style)}
          className={classes}
        />
      );
    } else {
      return (
        <div
          {...other}
          style={Object.assign(styles.root, style)}
          className={classes}
        >
          {icon && React.cloneElement(icon, {
            color: styles.icon.color,
            style: Object.assign(styles.icon, icon.props.style),
          })}
          {this.props.children}
        </div>
      );
    }
  }
}

export default Avatar;
