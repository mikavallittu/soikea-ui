import Colors from '../styles/colors';
import Theme from '../styles/theme';

const size = 40; // default size

const styles = {
  'avatar': {
    fontFamily: Theme.fontFamily,
    color: Colors.white,
    backgroundColor: Theme.colorBrandPrimary,
    borderWidth: 0,
    borderStyle: 'solid',
    borderColor: Colors.grayLighter,
    borderRadius: '50%',
    userSelect: 'none',
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    textTransform: 'uppercase',
    lineHeight: `${size}px`,
    fontSize: size / 2 - 4,
    height: size,
    width: size,

    '&.squared': {
      borderRadius: 0,
    }

  }
};

export default styles;
