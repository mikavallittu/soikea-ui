Avatars can be used to represent people or object.

    <div>
      <Avatar src="assets/mika_avatar.jpg" />
      <hr />

      <Avatar>
        MV
      </Avatar>
      <hr />

      <Avatar size={64}>
        MV
      </Avatar>
    </div>
