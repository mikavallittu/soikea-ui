import React from 'react';
import test from 'tape';
import { shallow } from 'enzyme';

import Component from "./Component";

test('<Component />', (t) => {
  const wrapper = shallow(<Component />);
  t.ok(true, 'should render something');
  t.end();
});
