import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import createStyleSheet from '../utils/createStyleSheet';
import styles from './Component.style';

const styleSheet = createStyleSheet(styles);

function getClasses(className){
  const classes = styleSheet.classes;
  return classNames(classes.component || 'component', className);
}

function getStyles(props, state) {
  const {
    disabled,
  } = props;

  const styles = {
    root: {

    }
  };

  return styles;
}

class Toggle extends Component {
  static propTypes = {
    /**
     * Will disable the component if true.
     */
    disabled: PropTypes.bool,

    /**
     * Override the inline-styles of the root element.
     */
    style: PropTypes.object,

  };

  static defaultProps = {
    disabled: false,
  };

  state = {

  };

  render() {
    const {
      children,
      disabled,
      style,
      className,
      ...other
    } = this.props;

    const styles = getStyles(this.props);
    const classes = getClasses(className);

    return (
      <div
        {...other}
        style={Object.assign(styles.root, style)}
        className={classes}
      >
        {children}
      </div>
    );
  }
}

export default Component;
