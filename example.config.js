// We use Webpack for serving the example/ with Babel and hot loader

module.exports = {

  entry: {
    js: "./example/app.js",
    html: "./example/index.html",
  },

  output: {
    filename: "app.js",
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ["react-hot-loader", "babel-loader"],
      },
      {
        test: /\.html$/,
        loader: "file?name=[name].[ext]",
      },
    ],
  },

}
