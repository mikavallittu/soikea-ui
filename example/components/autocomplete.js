import React, { Component } from 'react';
import AutoComplete from '../../src/AutoComplete';

function matchStateToTerm (state, value) {
  return (
    state.name.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
    state.value.toLowerCase().indexOf(value.toLowerCase()) !== -1
  )
}

function sortStates (a, b, value) {
  const aLower = a.name.toLowerCase();
  const bLower = b.name.toLowerCase();
  const valueLower = value.toLowerCase();
  const queryPosA = aLower.indexOf(valueLower);
  const queryPosB = bLower.indexOf(valueLower);

  if (queryPosA !== queryPosB) {
    return queryPosA - queryPosB;
  }
  return aLower < bLower ? -1 : 1;
}

function fakeRequest (value, cb) {
  if (value === '') return getStates()
  var items = getStates().filter((state) => {
    return matchStateToTerm(state, value)
  })
  setTimeout(() => {
    cb(items)
  }, 500)
}

function getStates() {
  return [
    {value: '01', tag: {label: 'sähköposti', color: '#7FDBFF'}, name: "SÄHKÖPOSTI"},
    {value: '02', tag: {label: 'tekstiviesti', color: '#7FDBFF'}, name: "TEKSTIVIESTI"},
    {value: '13', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "KERAVA"},
    {value: '14', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "TURKU"},
    {value: '15', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "PORI"},
    {value: '20', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "VAASA"},
    {value: '21', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "JYVÄSKYLÄ"},
    {value: '22', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "KUOPIO "},
    {value: '23', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "OULU"},
    {value: '26', tag: {label: 'hätäkeskus', color: '#FF4136'}, name: "PORVOON PELASTUSLAITOS"},
    {value: '31', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "TAMPEREEN VARTIOINTI"},
    {value: '32', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "G4S (FALCK)"},
    {value: '33', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "SECURITAS"},
    {value: '34', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "ISS"},
    {value: '35', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "NISCAYAH"},
    {value: '36', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "OTSO PALVELUT OY"},
    {value: '37', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "TURUN HÄLYTYSVALVONTA OY"},
    {value: '38', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "LINNANVARTIJAT HÄMEENLINNA"},
    {value: '39', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "TURVATIIMI"},
    {value: '40', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "LASSILA & TIKANOJA"},
    {value: '41', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "ETELÄVARTIOINTI OY"},
    {value: '42', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "ETELÄ-KARJALAN TURVAPALVELU KY"},
    {value: '43', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "VERIFI OY"},
    {value: '44', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "VARTIOINTI TANSKANEN OY JOENSUU"},
    {value: '45', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "PALMIA OY HELSINKI"},
    {value: '46', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "ANVIA KOKKOLA"},
    {value: '47', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "LIIKE- JA TEOLLISUUSVARTIOINTI OY RLTV"},
    {value: '48', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "TOTAL KIINTEISTÖPALVELUT OY JYVÄSKYLÄ"},
    {value: '49', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "MIKKELIN VARTIOINTIKESKUS OY"},
    {value: '50', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "YIT"},
    {value: '51', tag: {label: 'vartiointiliike', color: '#3D9970'}, name: "DNA OULU"},
    {value: '001', tag: {label: 'com-laji', color: '#FF851B'}, name: 'Linjavika'},
    {value: '002', tag: {label: 'com-laji', color: '#FF851B'}, name: '1.kiireellisyysluokan hälytys'},
    {value: '003', tag: {label: 'com-laji', color: '#FF851B'}, name: '2.kiireellisyysluokan hälytys'},
    {value: '004', tag: {label: 'com-laji', color: '#FF851B'}, name: 'Laitevika'},
    {value: '005', tag: {label: 'com-laji', color: '#FF851B'}, name: 'Turvavalvontahälytys'},
    {value: '006', tag: {label: 'com-laji', color: '#FF851B'}, name: '1.kiireellisyysluokan ylärajahälytys'},
    {value: '007', tag: {label: 'com-laji', color: '#FF851B'}, name: '1.kiireellisyysluokan alarajahälytys'},
    {value: '008', tag: {label: 'com-laji', color: '#FF851B'}, name: 'Laskinrajahälytys'},
    {value: '012', tag: {label: 'com-laji', color: '#FF851B'}, name: 'Väylähäiriö'},
    {value: '013',  name: '3.kiirellisyysluokan hälytys'},
    {value: '014',  name: '4.kiirellisyysluokan hälytys'},
  ]
}

function getStates2() {
  return [
    {value: '01', name: "SÄHKÖPOSTI"},
    {value: '02', name: "TEKSTIVIESTI"},
    {value: '13', name: "KERAVA"},
  ]
}

class Index extends Component {
  state = {
    value: ''
  };

  render() {
    return (
      <div>
        <AutoComplete
          labelText='Meno-osoite'
          inputProps={{placeholder: 'Etsi meno-osoitteita'}}
          tagStyle={{minWidth: 80}}
          disabled={false}
          value={this.state.value}
          items={getStates()}
          getItemValue={(item) => item.name}
          shouldItemRender={matchStateToTerm}
          sortItems={sortStates}
          onChange={(event, value) => this.setState({ value })}
          onSelect={value => this.setState({ value })}
          onClear={() => this.setState({ value: '' })}
        />
      </div>
    );
  }
}

export default Index;
