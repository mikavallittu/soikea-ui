import React from 'react';
import Button from '../../src/Button';
import Icon from '../../src/svg-icons/add';

export default () => {
  return (
    <section>
      <Button primary icon={<Icon />}>Hello</Button>
    </section>
  );
}
