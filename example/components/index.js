import React from 'react';
import Avatars from './avatars';
import Buttons from './buttons';
import Papers from './papers';
import Checkboxes from './checkboxes';
import Selects from './selects';
import Form from './form';
import Autocomplete from './autocomplete';
import Tabs from './tabs';

const styles = {
  container: {
    padding: 20,
  }
};

export default React.createClass({
  render() {
    return (
      <div style={styles.container}>
        {/*
        <Avatars />
        <hr />
        <Buttons />
          <hr />
        <Papers />
        <hr />
        <Checkboxes />
        <hr />
        <Selects />

        <Form />

        <Autocomplete />
        */}

        <Tabs />
      </div>
    );
  }
});
