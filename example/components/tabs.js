import React from 'react';
import { Tab, Tabs } from '../../src/Tabs';

export default () => {
  return (
    <section>
      <Tabs activeTab={2}>
        <Tab label="Tab 1">
          <div>My tab 1</div>
        </Tab>
        <Tab label="Tab 2">
          <div>My tab 2</div>
        </Tab>
        <Tab label="Tab XXXXX" badge="6">
          <div>My tab 3</div>
        </Tab>
      </Tabs>
    </section>
  );
}
