import React from 'react';
import Button from '../../src/Button';
import Input from '../../src/Input';
import Select from '../../src/Select';
import Dropdown from '../../src/Dropdown';

const styles = {
  group: {
    display: 'flex',
    alignItems: 'start',
  }
};

const selectOptions = [
  { value: 1, label: 'One' },
  { value: 2, label: 'Two' },
  {
    type: 'group', name: 'group1', items: [
      { value: 3, label: 'Three' },
      { value: 4, label: 'Four' }
    ]
  },
  {
    type: 'group', name: 'group2', items: [
      { value: 5, label: 'Five' },
      { value: 6, label: 'Six' }
    ]
  }
];

export default () => {
  return (
    <form>
      <div style={styles.group}>
        <Input
          labelText="Network"
          helpText="Combines supernet/subnet"
          inline
          style={{marginRight: 9}}/>

        <Dropdown
          labelText="Terminal"
          helpText="Number between: 5 - 255"
          options={[1, 2, 3, 4, 5]}
          inline />
      </div>

      <div style={styles.group}>
        <Select
          labelText='Filters'
          helpText='Select filter to be used'
          options={selectOptions} />
      </div>

      <div style={styles.group}>
        <Input
          labelText="Name"
          helpText="This can be left empty" />
      </div>

      <Button primary style={{marginTop: 9}}>Submit</Button>
    </form>
  );
}
