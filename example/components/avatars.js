import React from 'react';
import Avatar from '../../src/Avatar';

export default () => {
  return (
    <section>
      <Avatar>
        <span>MV</span>
      </Avatar>
      <Avatar squared={true} />
      <Avatar src={'/assets/mika_avatar.jpg'} />
      <Avatar src={'/assets/mika_avatar.jpg'} size={60} />
        <Avatar size={60}>
          <span>MV</span>
        </Avatar>
    </section>
  );
}
