import React from 'react';
import Select from '../../src/Select';

const options = [
  'one', 'three', 'five'
];

export default () => {
  return (
    <section>
      <Select labelText="Options" options={options} />
    </section>
  );
}
