#! /usr/bin/env node
/**
 * Create index.js for icons
 * =========================
 *
 * Usage:
 *
 * cd icon-builder/
 * node ./createIndex.js
 *
 */

var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var glob = require('glob');
var hb = require('handlebars');
var parseArgs = require('minimist');

var DEFAULT_OPTIONS = {
  glob: "/**/*Icon.js",
  dir:  "../src/icons/"
};

function main(options) {
  options = _.defaults(options, DEFAULT_OPTIONS);
  //console.log('Building src/icons/index.js', options);

  var files = glob.sync(options.dir + options.glob);
  files = files.map(function (path) {
    return parsePath(path, options);
  });

  createIndex(files, options);
}

function parsePath(iconPath, options) {
  var dir = iconPath.replace(options.dir, './');
  var name = path.basename(iconPath);

  // Format name to all lower case
  name = name.replace(".js", "");
  //name = name.toLowerCase();

  return {
    dir: dir,
    name: name
  }
}

function createIndex(files, options) {
  var template = fs.readFileSync(path.join(__dirname, 'templates/index.hbs'), {
    encoding: 'utf8',
  });
  template = hb.compile(template);

  var indexString = template({
    files: files
  });

  var fileName = options.dir + "index.js";
  fs.writeFileSync(fileName, indexString);
  console.log("Wrote:", fileName);
}

function buildString() {

}

if (require.main === module) {
  var argv = parseArgs(process.argv.slice(2));
  main(argv);
}
