#! /usr/bin/env node
/**
 * Soikea UI Icon Builder
 * ======================
 *
 * Usage:
 *
 * cd icon-builder/
 * node ./build.js
 *
 */

var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var glob = require('glob');
var hb = require('handlebars');
var parseArgs = require('minimist');

var DEFAULT_OPTIONS = {
  glob:      "/**/*.svg",
  inputDir:  "../assets/icons/svg",
  outputDir: "../assets/icons/dist"
};

function main(options) {
  options = _.defaults(options, DEFAULT_OPTIONS);
  console.log('Building icons...', options);

  var files = glob.sync(options.inputDir + options.glob);
  //var files = ["../src/icons/svg/Alert.svg"];

  files.forEach(function (path) {
    processFile(path, options);
  });
}

function parsePath(svgPath) {
  var data = path.parse(svgPath);
  var dirs = data.dir.split(path.sep);

  return {
    name: data.name,
    folder: _.last(dirs)
  };
}

function getIconData(svgPath) {
  var data = parsePath(svgPath);
  // Get name and type from the file path
  var name = data.name + "Icon";
  var type = data.folder;

  return {
    name: name,
    type: type
  };
}

function processFile(file, options) {
  var source = fs.readFileSync(file, {
    encoding: 'utf8',
  });

  var template = fs.readFileSync(path.join(__dirname, 'templates/Icon.hbs'), {
    encoding: 'utf8',
  });
  template = hb.compile(template);

  // Parse viewBox coordinates
  var viewBox = source.match(/viewBox="(.*?)"/);
  viewBox = viewBox[1] || "0 0 200 200";

  // Parse path information from the svg source
  source = source.replace(/[\n\r\t]/g, '');
  source = source.match(/<path .*?\/>/g);
  source = source.join('\n');

  var iconData = getIconData(file);
  var iconString = template({
    name: iconData.name,
    type: iconData.type,
    paths: source,
    viewBox: viewBox
  });

  var outputDir = options.outputDir + path.sep;
  var fileName = iconData.name + ".js";

  fs.writeFileSync(outputDir + fileName, iconString);
  console.log("Wrote:", fileName);
}

if (require.main === module) {
  var argv = parseArgs(process.argv.slice(2));
  main(argv);
}
